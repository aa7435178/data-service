/*
package org.data.api.repos;

import org.data.api.AbstractIntegrationTestForNews;
import org.data.api.news.News;
import org.data.api.news.NewsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NewsRepositoryTest extends AbstractIntegrationTestForNews {


  @Autowired
  private NewsRepository newsRepository;

  @Test
  void getNewsById() {
    News news = new News(null, "t1", "d1");
    newsRepository.save(news);
    newsRepository.getNewsById(news.getId());
    assertEquals(news.getId(), news.getId());
    newsRepository.delete(news.getId());
  }

  @Test
  void getListNews() {
    News news = new News(null, "t", "d");
    News news1 = new News(null, "t1", "d1");
    News news2 = new News(null, "t2", "d2");
    News savedNews = newsRepository.save(news);
    News savedNews1 = newsRepository.save(news1);
    News savedNews2 = newsRepository.save(news2);

  }

  @Test
  void save() {
    News news = new News(null, "t", "d");
    News savedNews = newsRepository.save(news);
    assertThat(savedNews).usingRecursiveComparison().ignoringFields("id").isEqualTo(news);
    newsRepository.delete(savedNews.getId());
  }

  @Test
  void update() {
    News news = new News(null, "t", "d");
    News savedNews = newsRepository.save(news);
    News buildNews = News.builder()
      .id(savedNews.getId())
      .title("t_u")
      .description("d_u")
      .build();
    News updatedNews = newsRepository.update(buildNews, savedNews.getId());
    assertThat(updatedNews).usingRecursiveComparison().ignoringFields("id").isEqualTo(buildNews);
    newsRepository.delete(updatedNews.getId());
  }

  @Test
  void delete() {
    News news = new News(null, "t", "d");
    News savedNews = newsRepository.save(news);
    newsRepository.delete(savedNews.getId());
  }
}
*/
