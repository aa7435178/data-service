package org.data.api.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class UserService {
  private final UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public User getUserById(Integer id) {
    if (id == 0 || id <= 0) {
      throw new RuntimeException("id must be more than zero");
    }
    return userRepository.getUserById(id);
  }

  public List<User> listUser(int offset, int limit, String order) {
    return userRepository.getUserList(offset, limit, order);
  }

  public User saveAndReturnUser(User user) {
    User saved;
    if (user.getId() != null) {
      saved = userRepository.update(user, user.getId());
    } else {
      saved = userRepository.save(user);
    }
    return saved;
  }

  public Map<String, String> delete(int id) {
    try {
      userRepository.delete(id);
      return Map.of("Status", "ok");
    } catch (Exception e) {
      log.info("delete", e);
      return Map.of("Status", "error");
    }
  }
}
