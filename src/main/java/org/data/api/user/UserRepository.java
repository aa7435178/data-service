package org.data.api.user;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

  private final JdbcTemplate jdbcTemplate;

  public UserRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public User getUserById(Integer id) {
    return jdbcTemplate.queryForObject("select * from users where id = ?", new BeanPropertyRowMapper<>(User.class), id);
  }
  public User getUserByEmail(String email) {
    return jdbcTemplate.queryForObject("select * from users where email = ?", new BeanPropertyRowMapper<>(User.class), email);
  }


  public List<User> getUserList(int offset, int limit, String order) {
    List<User> userList = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from users order by id, ? limit ? offset ?");
      statement.setString(1, order);
      statement.setInt(2, limit);
      statement.setInt(3, offset);

      var resultSet = statement.executeQuery();

      List<User> res = new ArrayList<>();

      if (resultSet.next()) {
        res.add(User.builder()
          .id(resultSet.getInt("id"))
          .email(resultSet.getString("email"))
          .password(resultSet.getString("password"))
          .creationDate(resultSet.getDate("creationdate"))
          .updateDate(resultSet.getDate("updatedate"))
          .build());
      }
      return res;


    });
    return userList;
  }

  public User save(User user) {
    Integer id = jdbcTemplate.execute((Connection con) -> {
        PreparedStatement statement = con.prepareStatement(
          "insert into users (email, password) values (?, ?) returning id;");
        statement.setString(1, user.getEmail());
        statement.setString(2, user.getPassword());
        ResultSet res = statement.executeQuery();
        if (!res.next()) {
          return null;
        } else {
          return res.getInt("id");
        }
      }
    );
    return new User(id, user.getEmail(), user.getPassword(), user.getCreationDate(), user.getUpdateDate(), user.getBirthday());
  }

  public User update(User user, Integer id) {
    jdbcTemplate.update("UPDATE users set email=?, password=?, creationdate=?, updatedate=? where id=?",
      user.getEmail(), user.getPassword(), user.getBirthday(), user.getUpdateDate(), user.getCreationDate(), id);
    return user;
  }

  public void delete(int id) {
    jdbcTemplate.update("delete from users where id=?", id);
  }
}
