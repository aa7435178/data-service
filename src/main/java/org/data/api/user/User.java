package org.data.api.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
  private Integer id;
  private String email;
  private String password;
  private Date birthday;
  private Date creationDate;
  private Date updateDate;


}
