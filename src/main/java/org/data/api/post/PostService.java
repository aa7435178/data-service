package org.data.api.post;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.data.api.post.request.UpdatePostRequest;
import org.data.api.post.responce.PostResponce;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
@Slf4j
public class PostService {

  private final PostRepository postRepository;

  private static final List<PostResponce> POST_LIST_FROM_USER = new ArrayList<>();

  public Post getPostById(Integer id) {
    if (id == 0 || id <= 0) {
      throw new RuntimeException("id must be more than zero");
    }
    return postRepository.getPostById(id);
  }

  public PostResponce addPost(Post post) {
    postRepository.save(post);
    return PostResponce.fromPost(post);
  }

  public PostResponce updatePost(Integer userId, UpdatePostRequest request) {

    Post postFromDB = postRepository.getPostById(request.getId());
    if (postFromDB == null) {
      throw new RuntimeException("post not found");
    }
    if (!userId.equals(postFromDB.getUserId())) {
      throw new RuntimeException("you can`t update this post");
    }
    postFromDB.setText(request.getText());

    postRepository.update(postFromDB, postFromDB.getId());

    return PostResponce.fromPost(postFromDB);

  }

  public Map<String, String> delete(int id, Integer userId) {
    Post postFromDB = postRepository.getPostById(id);
    if (postFromDB == null) {
      throw new RuntimeException(" post not found");
    }
    if (!userId.equals(postFromDB.getUserId())) {
      throw new RuntimeException("you can`t delete this post");
    }
    postRepository.delete(id);
    return Map.of("Status", "ok");
  }

  public Map<String, Object> list(Integer userId, int offset, int limit, String order) {
    List<Post> list = postRepository.list(userId, offset, limit + 1, order);
    if (list.size() > limit) {
      return Map.of(
        "status", "ok",
        "hasNext", true,
        "data", list.stream().limit(limit).collect(Collectors.toList())
      );

    } else {
      return Map.of(
        "status", "ok",
        "hasNext", false,
        "data", list
      );
    }
  }
}
