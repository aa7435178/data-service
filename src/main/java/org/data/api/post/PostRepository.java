package org.data.api.post;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
@AllArgsConstructor
public class PostRepository {
  private final JdbcTemplate jdbcTemplate;

  public Post save(Post addedPost) {
    Integer id = jdbcTemplate.execute((Connection con) -> {
        PreparedStatement statement = con.prepareStatement("insert into posts (user_id, text, creation_date, update_date) values (?, ?, ?, ?) returning id;");
        statement.setInt(1, addedPost.getUserId());
        statement.setString(2, addedPost.getText());
        statement.setDate(3, new java.sql.Date(addedPost.getCreationDate().getTime()));
        statement.setDate(4, new java.sql.Date(addedPost.getUpdateDate().getTime()));
        ResultSet res = statement.executeQuery();
        if (!res.next()) {
          return null;
        } else {
          return res.getInt("id");
        }
      }
    );
    addedPost.setUserId(id);
    return addedPost;
  }

  public Post update(Post updated, int id) {
    jdbcTemplate.update("UPDATE posts set  text=?, update_date=? where id=?", updated.getText(), updated.getUpdateDate(), id);
    return updated;
  }

  public void delete(int id) {
    jdbcTemplate.update("delete from posts where id=?", id);
  }

  public List<Post> list(Integer userId, int offset, int limit, String order) {
    List<Post> postsSet = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select all * from posts where user_id=? order by creation_date, ? limit ? offset ? ");
      statement.setInt(1, userId);
      statement.setString(2, order);
      statement.setInt(3, limit);
      statement.setInt(4, offset);


      var postList = statement.executeQuery();

      List<Post> res = new ArrayList<>();

      while (postList.next()) {
        res.add(Post.builder()
          .id(postList.getInt("id"))
          .userId(postList.getInt("user_id"))
          .text(postList.getString("text"))
          .creationDate(postList.getDate("creation_date"))
          .updateDate(postList.getDate("update_date"))
          .build());
      }

      return res;


    });

    return postsSet;
  }

  public Post getPostById(Integer id) {
    Post post = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from posts where id = ?");
      statement.setInt(1, id);

      var resultSet = statement.executeQuery();

      var res = new Post();
      if (resultSet.next()) {
        res.setId(resultSet.getInt("id"));
        res.setUserId(resultSet.getInt("user_id"));
        res.setText(resultSet.getString("text"));
        res.setCreationDate(resultSet.getDate("creation_date"));
        res.setUpdateDate(resultSet.getDate("update_date"));
      }
      return res;
    });
    return post;
  }
}

