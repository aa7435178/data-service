package org.data.api.config;
import io.swagger.v3.oas.models.Components;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.AllArgsConstructor;

import org.data.api.jwt.JwtService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import static org.data.api.utils.ApiUtil.AUTHORIZATION;

@Configuration
@AllArgsConstructor
public class OpenApiConfig {
  private final JwtService jwtService;

  @Bean
  OpenAPI customOpenApi(@Value("${application-description:test}") String appDescription,
                        @Value("${application-version:first}") String appVersion) {


    final String securitySchemeName = AUTHORIZATION;
    final String apiTitle = String.format("%s API", StringUtils.capitalize("com.voice"));


    return new OpenAPI()
      .info(
        new Info().title("Application API")
          .version(appVersion)
          .description(appDescription)
          .license(
            new License()
              .name("Apache 2.0")
              .url("http://springdoc.org")
          )
          .contact(
            new Contact()
              .name("username")
              .email("test@gmail.com"))
      )
      .components(
        new Components()
          .addSecuritySchemes(
            securitySchemeName,
            new SecurityScheme()
              .name(securitySchemeName)
              .type(SecurityScheme.Type.APIKEY)
              .in(SecurityScheme.In.HEADER)
          )
      );
  }
}
