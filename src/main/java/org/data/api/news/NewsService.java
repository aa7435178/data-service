package org.data.api.news;

import lombok.extern.slf4j.Slf4j;
import org.data.api.news.request.UpdateNewsRequest;
import org.data.api.news.response.NewsResponse;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class NewsService {

  private final NewsRepository newsRepository;


  private static final List<NewsResponse> TOP_10_LIST = new ArrayList<>();

  public NewsService(NewsRepository newsRepository) {
    this.newsRepository = newsRepository;
  }

  public News getNewsById(Integer id) {
    if (id == 0 || id <= 0) {
      throw new RuntimeException("id must be more than zero");
    }
    return newsRepository.getNewsById(id);
  }

  public Map<String, Object> listNews(int offset, int limit, String order) {
    List<News> list = newsRepository.getListNews(offset, limit + 1, order);
    if (list.size() > limit) {
      return Map.of(
        "status", "ok",
        "hasNext", true,
        "data", list.stream().limit(limit).collect(Collectors.toList())
      );

    } else {
      return Map.of(
        "status", "ok",
        "hasNext", false,
        "data", list
      );
    }
  }

  @PostConstruct()
  public void initHotNews() {
    var executor = Executors.newSingleThreadScheduledExecutor();
    executor.scheduleWithFixedDelay(() -> {
      var list = newsRepository.get10HotNews();
      TOP_10_LIST.clear();
      TOP_10_LIST.addAll(list);
    }, 0, 30, TimeUnit.SECONDS);


  }

  public List<NewsResponse> get10HotNews() {
    if (TOP_10_LIST.isEmpty()) {
      var list = newsRepository.get10HotNews();
      TOP_10_LIST.addAll(list);
      return TOP_10_LIST;
    } else {
      return TOP_10_LIST;
    }
  }


  public NewsResponse create(News news) {
    newsRepository.save(news);

    return NewsResponse.fromNews(news);
  }

  public Map<String, String> delete(int id, Integer userId) {
    News newsFromDB = newsRepository.getNewsById(id);
    if (newsFromDB == null) {
      throw new RuntimeException("news not found");
    }
    if (!userId.equals(newsFromDB.getUserId())) {
      throw new RuntimeException("you can`t delete this post");
    }
    newsRepository.delete(id);
    return Map.of("Status", "ok");
  }

  public NewsResponse updateNews(Integer userId, UpdateNewsRequest request) {

    News newsFromDB = newsRepository.getNewsById(request.getId());
    if (newsFromDB == null) {
      throw new RuntimeException("news not found");
    }
    if (!userId.equals(newsFromDB.getUserId())) {
      throw new RuntimeException("you can`t update this news");
    }
    newsFromDB.setTitle(request.getTitle());
    newsFromDB.setDescription(request.getDescription());
    newsFromDB.setUpdateDate(new Date());

    newsRepository.update(newsFromDB, newsFromDB.getId());

    return NewsResponse.fromNews(newsFromDB);
  }
}

