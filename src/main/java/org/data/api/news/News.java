package org.data.api.news;


import lombok.*;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class News {
  private Integer id;
  private Integer userId;
  private String title;
  private String description;
  private Date creationDate;
  private Date updateDate;
}
