package org.data.api.profile.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.data.api.profile.UserProfile;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileResponse {
  private Integer userId;
  private String nickname;
  private String about;
  private Date birthday;

  public static UserProfileResponse fromProfile(UserProfile userProfile) {
    return UserProfileResponse.builder()
      .userId(userProfile.getUserId())
      .nickname(userProfile.getNickname())
      .about(userProfile.getAbout())
      .birthday(userProfile.getBirthday())
      .build();
  }
}
