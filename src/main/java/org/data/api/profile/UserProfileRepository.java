package org.data.api.profile;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserProfileRepository {

  private final JdbcTemplate jdbcTemplate;

  public UserProfileRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public UserProfile getProfileById(Integer id) {
    UserProfile profile = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from user_profile where id = ?");
      statement.setInt(1, id);

      var resultSet = statement.executeQuery();

      var res = new UserProfile();
      while (resultSet.next()) {
        res.setId(resultSet.getInt("id"));
        res.setUserId(resultSet.getInt("user_id"));
        res.setBirthday(resultSet.getTime("birthday"));
        res.setAbout(resultSet.getString("about"));
      }
      return res;
    });
    return profile;
  }

  public UserProfile getProfileByUserId(Integer userId) {
    UserProfile profile = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from user_profile where user_id = ?");
      statement.setInt(1, userId);

      var resultSet = statement.executeQuery();

      var res = new UserProfile();
      if (resultSet.next()) {
        res.setId(resultSet.getInt("id"));
        res.setUserId(resultSet.getInt("user_id"));
        res.setBirthday(resultSet.getTime("birthday"));
        res.setAbout(resultSet.getString("about"));
      }
      return res;
    });
    return profile;
  }

  public List<UserProfile> getProfileList(int offset, int limit, String order) {
    List<UserProfile> profileList = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from user_profile order by user_id, ? limit ? offset ?");
      statement.setString(1, order);
      statement.setInt(2, limit);
      statement.setInt(3, offset);

      var resultSet = statement.executeQuery();

      List<UserProfile> res = new ArrayList<>();

      while (resultSet.next()) {
        res.add(UserProfile.builder()
          .id(resultSet.getInt("id"))
          .userId(resultSet.getInt("user_id"))
          .about(resultSet.getString("about"))
          .birthday(resultSet.getTime("birthday"))
          .build());
      }
      return res;
    });
    return profileList;
  }

  public UserProfile save(UserProfile profile) {
    Integer id = jdbcTemplate.execute((Connection con) -> {
        PreparedStatement statement = con.prepareStatement(
          "insert into user_profile (user_id, about, birthday, nickname) values (?, ?, ?, ?) returning id;");
        statement.setInt(1, profile.getUserId());
        statement.setString(2, profile.getAbout());
        statement.setDate(3, new java.sql.Date(profile.getBirthday().getTime()));
        statement.setString(4, profile.getNickname());
        ResultSet res = statement.executeQuery();
        if (!res.next()) {
          return null;
        } else {
          return res.getInt("id");
        }
      }
    );
    profile.setUserId(id);
    return profile;
  }


  public UserProfile update(UserProfile profile, Integer id) {
    jdbcTemplate.update("UPDATE user_profile set  user_id=?, birthday=?, about=?, nickname=? where id=?",
      profile.getUserId(), profile.getBirthday(), profile.getAbout(), id);
    return profile;
  }

  public void delete(int id) {
    jdbcTemplate.update("delete from user_profile where id=?", id);
  }

}
