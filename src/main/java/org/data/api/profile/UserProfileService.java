package org.data.api.profile;

import lombok.extern.slf4j.Slf4j;
import org.data.api.profile.request.UpdateProfileRequest;
import org.data.api.profile.response.UserProfileResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class UserProfileService {
  private final UserProfileRepository userProfileRepository;

  public UserProfileService(UserProfileRepository userProfileRepository) {
    this.userProfileRepository = userProfileRepository;
  }


  public UserProfile getProfileById(Integer id) {
    if (id == 0 || id <= 0) {
      throw new RuntimeException("id must be more than zero");
    }
    return userProfileRepository.getProfileById(id);
  }

  public UserProfile getProfileByUserId(Integer userId) {
    if (userId == 0 || userId <= 0) {
      throw new RuntimeException("userId must be more than zero");
    }
    return userProfileRepository.getProfileByUserId(userId);
  }

  public List<UserProfile> profileList(int offset, int limit, String order) {
    return userProfileRepository.getProfileList(offset, limit, order);
  }

  public UserProfile save(UserProfile profile) {
    UserProfile saved;
    if (profile.getId() != null) {
      saved = userProfileRepository.update(profile, profile.getId());
    } else {
      saved = userProfileRepository.save(profile);
    }
    return saved;
  }

  public Map<String, String> delete(int id) {
    try {
      userProfileRepository.delete(id);
      return Map.of("Status", "ok");
    } catch (Exception e) {
      log.info("delete", e);
      return Map.of("Status", "error");
    }
  }


  public UserProfileResponse update(Integer userId, UpdateProfileRequest profileRequest) {

    var userProfile = new UserProfile();
    userProfile.setNickname(profileRequest.getNickname());
    userProfile.setAbout(profileRequest.getAbout());
    userProfile.setBirthday(profileRequest.getBirthday());

    userProfileRepository.update(userProfile, userId);

    return UserProfileResponse.fromProfile(userProfile);


  }


}
